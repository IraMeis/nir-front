import axios from "axios";
import back from "../util/url.js";
import api from "../util/apiList.json";

const addr = `${back.baseUrl}`

const API_URL_IMG = addr + api.runIMG;
const API_URL_VI = addr + api.runVI;
const API_URL_VI_EVAL = addr + api.evalVI;
const API_URL_IMG_EVAL = addr + api.evalIMG;

const API_URL_SAVE = addr + api.save;
const API_URL_GET_EVALS = addr + api.getEvals;

const uploadImage = (file, netName) => {
    let formData = new FormData();
    formData.append("image", file);
    return axios.post(API_URL_IMG + '/' + netName, formData, {
        responseType: 'blob',
        headers: {
            'Content-Type': 'image/jpeg'
        }
    })
};

const uploadVideo = (file, netName) => {
    let formData = new FormData();
    formData.append("video", file);
    return axios.post(API_URL_VI + '/' + netName, formData, {
        responseType: 'blob',
        headers: {
                'Content-Type': 'video/mp4'
        }
    });
};

const getAdditionalDataVideo = (uuid) => {
    return axios.get(API_URL_VI_EVAL + '/' + uuid);
};

const getAdditionalDataImage = (uuid) => {
    return axios.get(API_URL_IMG_EVAL + '/' + uuid);
};

const saveEval = (data) => {
    return axios.post(API_URL_SAVE, data);
};

const getEvals = (search) => {
    return axios.put(API_URL_GET_EVALS + search);
};

const UploadService = {
    uploadVideo,
    uploadImage,
    getAdditionalDataVideo,
    getAdditionalDataImage,
    saveEval,
    getEvals
};

export default UploadService;